<!DOCTYPE html>
<html>

<head>

    <title>DailyTrends</title>
    <meta charset="UTF-8">
    <link rel="icon" href="img/favicon.png" type="image/gif" sizes="16x16">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <?php 
        include("functions.php");
        include("DOM/simple_html_dom.php");
    ?>

</head>

<body id="index">

    <main class="container pb-4">
        <div class="row py-4">
            <div class="col-12 text-center">
                <a href="index.php"><img src="img/logo.png" class="img-fluid" alt="DailyTrends Logo" width="550"></a>
            </div>
        </div>
        
        <!-- "EL MUNDO" -->
        <div class="row pt-4">
            <div class="col-12">
                <h2 class="newspaper">El Mundo</h2>
            </div>
        </div>
        
        <!-- Su primera noticia como destacada -->
        <div class="row">
        <?php
        
        $html = file_get_html('https://www.elmundo.es');
        $news_content = $html->find('div[class=ue-c-cover-content__body]');
        $i = 1;

        foreach($news_content as $news_res) {
            
            
        $article_title = $news_res->find("a[class=ue-c-cover-content__link]",0);
        $article_publisher = $news_res->find(".ue-c-cover-content__byline-name",0);
        $article_source = $news_res->find(".ue-c-cover-content__byline-location",0);
            
        $html_img = file_get_html($article_title->href);
        $article_image = $html_img->find("img[class=ue-c-article__media--image]",0);
            
            if($i<=1){
                
                if($article_image==null){
                    
                    echo '
                        <div class="pt-2 col-12 news-top-title">'.$article_title.'<span class="news-source"><b>EL MUNDO</b> |'.$article_publisher->outertext.'</span></div>';


                } else {
                    echo '
                        <div class="pt-2 col-6 news-top-title">'.$article_title.'<span class="news-source"><b>EL MUNDO</b> |'.$article_publisher->outertext.'</span></div>

                        <a class="col-6" href="'.$article_title->href.'"><img class="news-img-top" src="'.$article_image->src.'" alt="Imagen noticia"></a>';
                }
                
                $i++;
                
                
                
            } else {
                break;
            }
            
        }
        
        
        ?>
        </div>
        
        <hr class="my-3">
        
        <!-- Cuatro más de sus últimas 5 tras mostrar la primera como "destacadas" -->
        <div class="row">
        <?php
        

        foreach(array_slice($news_content,1) as $news_res) {
           
        $article_title = $news_res->find("a[class=ue-c-cover-content__link]",0);
        $article_publisher = $news_res->find(".ue-c-cover-content__byline-name", 0);
        $article_source = $news_res->find(".ue-c-cover-content__byline-location", 0);
            
        $html_img = file_get_html($article_title->href);
        $article_image = $html_img->find("img[class=ue-c-article__media--image]",0);
            
            
            if($i>1 && $i<=5) {
                
                echo '<div class="col-6 col-lg-3 pb-3">
                                    <div class="card">';
                                    
                                
            
                if($article_image==null){
                    echo    '
                        <div class="card-body news-padding">
                            <div class="card-title news-bot-title ">'.$article_title.'</div>
                                <p id="news-body" class="card-text">
                            </div>
                            <div class="card-footer news-padding">
                                <div class="news-source"><b>EL MUNDO</b> |'.$article_publisher->outertext.'</div>
                                </div>
                            </div>
                        </div>';


                } else {
                    echo    '<a href="'.$article_title->href.'"><img id="news-img" class="card-img-top" src="'.$article_image->src.'" alt="Imagen noticia"></a>
                                            <div class="card-body news-padding">
                                                <div class="card-title news-bot-title ">'.$article_title.'</div>
                                                <p id="news-body" class="card-text">
                                            </div>
                                            <div class="card-footer news-padding">
                                                <div class="news-source"><b>EL MUNDO</b> |'.$article_publisher->outertext.'</div>
                                            </div>
                                        </div>
                                    </div>';
                }
                
                $i++;
                
            } else {
                break;
            }
            
            
        }
            
        $html->clear();
        unset($html);
        $html_img->clear();
        unset($html_img);
        
        
        ?>
        </div>
        <!-- FIN DE "EL MUNDO" -->
        
        
        <!-- EL PAÍS -->
        <div class="row pt-4">
            <div class="col-12">
                <h2 class="newspaper">El País</h2>
            </div>
        </div>
        
        <!-- Su primera noticia como destacada -->
        <div class="row">
        <?php
        
        $html2 = file_get_html('https://elpais.com/');
        $news_content2 = $html2->find('div[class=articulo__interior]');
        $i2 = 1;

        foreach($news_content2 as $news_res2) {
            
        $article_title2 = $news_res2->find("h2[class=articulo-titulo]",0);
        $article_publisher2 = $news_res2->find(".firma",0);
        $article_src = $news_res2->find("a",0);
        $article_image2 = $news_res2->find("meta[itemprop=url]",0);
            
            if($i2<=1){
                
                if($article_image2==null){
                    
                    echo '
                        <div class="pt-2 col-12 news-top-title">'.$article_title2.'<span class="news-source"><b>EL PAÍS</b> | '.$article_publisher2->plaintext.'</span></div>';
                    
                    
                } else {
                    
                    echo '
                        <div class="pt-2 col-6 news-top-title">'.$article_title2.'<span class="news-source"><b>EL PAÍS</b> | '.$article_publisher2->plaintext.'</span></div>

                        <a class="col-6" href="'.$article_src->href.'"><img class="news-img-top" src="'.$article_image2->content.'" alt="Imagen noticia"></a>';
                    
                    
                    
                }
                
                $i2++;
                
                
            } else {
                break;
            }
            
        }
        
        
        ?>
        </div>
        
        <hr class="my-3">
        
        <!-- Cuatro más de sus últimas 5 tras mostrar la primera como "destacadas" -->
        <div class="row">
        <?php
        

        foreach(array_slice($news_content2,1) as $news_res2) {
           
        $article_title2 = $news_res2->find("h2[class=articulo-titulo]",0);
        $article_publisher2 = $news_res2->find("span[class=autor-nombre]",0);
        $article_src = $news_res2->find("a",0);
        $article_image2 = $news_res2->find("meta[itemprop=url]",0);
            
            if($i2>1 && $i2<=5) {
                
                echo '<div class="col-6 col-lg-3 pb-3">
                        <div class="card">
                            <div class="card-body news-padding">
                                <div class="card-title news-bot-title ">'.$article_title2.'</div>
                                <p id="news-body" class="card-text">
                            </div>
                            <div class="card-footer news-padding">
                                <div class="news-source"><b>EL PAÍS</b> | '.$article_publisher2->plaintext.'</div>
                            </div>
                        </div>
                    </div>';
                    
                $i2++;
                
            } else {
                break;
            }
            
            
        }
            
        $html2->clear();
        unset($html2);
        
        
        ?>
        </div>
        <!-- FIN DE "EL PAÍS" -->
        
        
        <!-- NOTICIAS PROPIAS -->
        <div class="row pt-5">
            <div class="col-12">
                <h2 class="newspaper">Otras noticias</h2>
                <a href="create.php" class="btn btn-success">Crear noticia</a>
            </div>
        </div>
        <div class="row pt-4">
                    <?php

                    $pdo = Database::connect();
                    $sql = 'SELECT * FROM news ORDER BY id_news DESC';
    
    

                    foreach ($pdo->query($sql) as $row) {
                        $text = strlen($row['text']) > 250 ? substr($row['text'],0,250)."..." : $row['text'];
                        
                        if($row['image']==null){
                            
                            echo '<div class="col-12 col-md-6 pb-3">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 id="news-title" class="card-title news-custom-title">'.$row['title'].'</h4>
                                            <p id="news-body" class="card-text">'.$text.'
                                            <a href="read.php?id='.$row['id_news'].'">Leer más</a></p>
                                        </div>
                                        <div class="card-footer">
                                            <div id="news-source" class="float-left">'.$row['source'].'</div>
                                            <div id="news-publisher" class=" float-right">'.$row['publisher'].'</div>
                                        </div>
                                    </div>
                                </div>';
                            
                        } else {
                            
                            echo '<div class="col-12 col-md-6 pb-3">
                                    <div class="card">
                                    
                                        <a href="read.php?id='.$row['id_news'].'"><img id="news-img" class="card-img-top" src="'.$row['image'].'" alt="Imagen noticia 1"></a>
                                        <div class="card-body">
                                            <h4 id="news-title" class="card-title news-custom-title">'.$row['title'].'</h4>
                                            <p id="news-body" class="card-text">'.$text.'
                                            <a href="read.php?id='.$row['id_news'].'">Leer más</a></p>
                                        </div>
                                        <div class="card-footer">
                                            <div id="news-source" class="float-left">'.$row['source'].'</div>
                                            <div id="news-publisher" class=" float-right">'.$row['publisher'].'</div>
                                        </div>
                                    </div>
                                </div>';
                            
                            
                        }
                            
                        
                            
                        }

                    Database::disconnect();
                    
                    ?>
        </div>
        <!-- FIN DE "NOTICIAS PROPIAS" -->
        
    </main>

    <footer class="container-fluid text-center bg-secondary">
        <div id="footer-logo" class="row py-3">
            <div class="col-12">
                <a href="index.php"><img src="img/logo.png" class="img" alt="DailyTrends Logo" width="250"></a>
            </div>
        </div>
        <div class="row py-2">
            <div class="col-12 d-flex justify-content-center">
                <ul class="list-group list-group-horizontal">
                    <li class="list-group-item bg-secondary border-0"><a class="text-white" href="#">Política de cookies</a></li>
                    <li class="list-group-item bg-secondary border-0"><a class="text-white" href="#">Política de privacidad</a></li>
                    <li class="list-group-item bg-secondary border-0"><a class="text-white" href="#">Términos y condiciones de uso</a></li>
                </ul>
            </div>
        </div>
    </footer>
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>


</body>

</html>