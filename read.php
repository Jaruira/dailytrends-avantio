<!DOCTYPE html>
<html>

<head>

    <title>DailyTrends</title>
    <meta charset="UTF-8">
    <link rel="icon" href="img/favicon.png" type="image/gif" sizes="16x16">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    
            <?php
                include('functions.php');
                $id = null;
                if ( !empty($_GET['id'])) {
                    $id = $_REQUEST['id'];
                }

                if ( null==$id ) {
                    header("Location: index.php");
                } else {
                    $pdo = Database::connect();

                    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    $sql = "SELECT * FROM news where id_news = ?";
                    $q = $pdo->prepare($sql);
                    $q->execute(array($id));
                    $row = $q->fetch(PDO::FETCH_ASSOC);

                    Database::disconnect();
                }
            ?>
    
</head>

<body id="read">

    <main class="container pb-4">
        <div class="row py-4">
            <div class="col-12 text-center">
                <a href="index.php"><img src="img/logo.png" class="img-fluid" alt="DailyTrends Logo" width="550"></a>
            </div>
        </div>
        
        <div class="row pt-4">
            <div class="col-12 pb-2 news-read-title"><?php echo "<h1>".$row['title']."</h1>"; ?></div>
        </div>
        
        <div class="row align-items-center">
            <div class="col-6"><?php echo "<small>".$row['source']." | ".$row['publisher']."</small>"; ?></div>
            <div class="col-6">
                <a href="delete.php?id=<?php echo $row['id_news']?>" class="btn btn-danger float-right text-white">Eliminar</a>
                <a href="update.php?id=<?php echo $row['id_news']?>" class="btn btn-primary float-right text-white mr-2">Editar</a>
            </div>
        </div>
        
        <hr>

        <div class="row pt-4">
            <div class="col-md-12 col-lg-8">
                <div class="col-12 pb-4"><?php echo "<img src='".$row['image']."' style='width:100%;'>"; ?></div>
                <div class="col-12"><?php echo "<p>".$row['text']."</p>"; ?></div>
            </div>
            <div class="col-4 d-none d-lg-block">
                <h4 class="col-12" id="other-news-title">Te puede interesar</h4>
                <?php 
                
                $pdo = Database::connect();

                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $sql = "SELECT * FROM news WHERE NOT id_news = ".$row['id_news']." ORDER BY RAND() LIMIT 1";
                $q = $pdo->prepare($sql);
                $q->execute();
                $other = $q->fetch(PDO::FETCH_ASSOC);

                $text2 = strlen($other['text']) > 150 ? substr($other['text'],0,110)."..." : $other['text'];
                
                echo '<div class="col-12">
                        <div class="card">
                            <a href="read.php?id='.$other['id_news'].'"><img id="new-img" class="card-img-top" src="'.$other['image'].'" alt="Imagen noticia 1"></a>
                            <div class="card-body">
                                <h4 id="new-title" class="card-title">'.$other['title'].'</h4>
                                <p id="new-body" class="card-text">'.$text2.'
                                <a href="read.php?id='.$other['id_news'].'">Leer más</a></p>
                            </div>
                            <div class="card-footer">
                                <div id="new-source" class="float-left">'.$other['source'].'</div>
                                <div id="new-publisher" class=" float-right">'.$other['publisher'].'</div>
                            </div>
                        </div>
                      </div>';
                
                
                Database::disconnect();
                
                ?>
            </div>
        </div>
        
        
    </main>

    <footer class="container-fluid text-center bg-secondary">
        <div id="footer-logo" class="row py-3">
            <div class="col-12">
                <img src="img/logo.png" class="img" alt="DailyTrends Logo" width="250">
            </div>
        </div>
        <div class="row py-2">
            <div class="col-12 d-flex justify-content-center">
                <ul class="list-group list-group-horizontal">
                    <li class="list-group-item bg-secondary border-0"><a class="text-white" href="#">Política de cookies</a></li>
                    <li class="list-group-item bg-secondary border-0"><a class="text-white" href="#">Política de privacidad</a></li>
                    <li class="list-group-item bg-secondary border-0"><a class="text-white" href="#">Términos y condiciones de uso</a></li>
                </ul>
            </div>
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>


</body>

</html>